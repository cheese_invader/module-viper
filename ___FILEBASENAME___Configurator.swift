//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import UIKit

class ___VARIABLE_productName:identifier___Configurator: ConfiguratorProtocol {
    // MARK: - ConfiguratorProtocol
    var view: UIViewController
    
    // MARK: - init
    init() {
        let interactor = ___VARIABLE_productName:identifier___Interactor()
        let router = ___VARIABLE_productName:identifier___Router()
        let presenter = ___VARIABLE_productName:identifier___Presenter(interactor: interactor, router: router)
        let view = ___VARIABLE_productName:identifier___View(presenter: presenter)

        interactor.setup(presenter: presenter)
        router.setup(presenter: presenter)
        presenter.setup(view: view)

        self.view = view
    }
}
