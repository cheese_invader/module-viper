//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

class ___VARIABLE_productName:identifier___Interactor: ___VARIABLE_productName:identifier___InteractorProtocol {
    // MARK: - VIPER
    private weak var presenter: ___VARIABLE_productName:identifier___PresenterInteractorProtocol?
    
    /// ConfirmConfigurator only
    func setup(presenter: ___VARIABLE_productName:identifier___PresenterInteractorProtocol?) {
        self.presenter = presenter
    }
    
    // MARK: - ___VARIABLE_productName:identifier___InteractorProtocol
}
