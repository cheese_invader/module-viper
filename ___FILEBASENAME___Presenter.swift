//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

class ___VARIABLE_productName:identifier___Presenter: ___VARIABLE_productName:identifier___PresenterViewProtocol, ___VARIABLE_productName:identifier___PresenterInteractorProtocol, ___VARIABLE_productName:identifier___PresenterRouterProtocol {
    // MARK: - VIPER
    private weak var view: ___VARIABLE_productName:identifier___ViewProtocol?
    private let interactor: ___VARIABLE_productName:identifier___InteractorProtocol
    private let router: ___VARIABLE_productName:identifier___RouterProtocol
    
    init(interactor: ___VARIABLE_productName:identifier___InteractorProtocol, router: ___VARIABLE_productName:identifier___RouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    /// ConfirmConfigurator only
    func setup(view: ___VARIABLE_productName:identifier___ViewProtocol) {
        self.view = view
    }
    
    // MARK: - ___VARIABLE_productName:identifier___PresenterViewProtocol
    // MARK: - ___VARIABLE_productName:identifier___PresenterInteractorProtocol
    // MARK: - ___VARIABLE_productName:identifier___PresenterRouterProtocol
}
