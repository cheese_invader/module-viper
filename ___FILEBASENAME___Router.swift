//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

class ___VARIABLE_productName:identifier___Router: ___VARIABLE_productName:identifier___RouterProtocol {
    // MARK: - VIPER
    private weak var presenter: ___VARIABLE_productName:identifier___PresenterRouterProtocol?
    
    /// ConfirmConfigurator only
    func setup(presenter: ___VARIABLE_productName:identifier___PresenterRouterProtocol) {
        self.presenter = presenter
    }
    
    // MARK: - ___VARIABLE_productName:identifier___RouterProtocol
}
